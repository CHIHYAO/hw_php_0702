
public class animal {
	public static abstract class Animal{
		String name;
		public Animal() {
			name = "Animal";
		}
		public void setname(String n) {
			name = n;
		}
		public abstract void move();
		public abstract void sound();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Animal a = 
				new Animal() {
					public void move() {
						System.out.println("Move: walk");
					}
					public void sound() {
						System.out.println("Sound: meow");
					}
				};
				a.move();
				a.sound();
	}
}
