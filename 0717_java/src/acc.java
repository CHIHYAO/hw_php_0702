import java.util.*;

public class acc {
	public int balance;
    public String name;
    
    public acc(String n, Integer b) {
        this.name = n; 
        this.balance = b;
    }
    
    @Override
    public String toString() {
        return String.format("account(%s, %d)", name, balance);
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<acc> arr = new ArrayList<acc>();
		acc a1 = new acc("Mary",30);
		acc a2 = new acc("Gary",78);
		acc a3 = new acc("Tery",66);
		acc a4 = new acc("Ben",1200);
		acc a5 = new acc("Alice",18);

		arr.add(a1);
		arr.add(a2);
		arr.add(a3);
		arr.add(a4);
		arr.add(a5);
		
		System.out.println("Inital sort: "+arr);
		
		arr.sort(Comparator.<acc,String>comparing(p ->p.name));
        System.out.println("Sorted by name: "+arr);
        arr.sort(Comparator.<acc,Integer>comparing(p ->p.balance));
        System.out.println("Sorted by balance: "+arr);
		
	}

}