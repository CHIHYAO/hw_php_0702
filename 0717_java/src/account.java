import java.util.Scanner;

public class account {
	public static class Account{
		public double balance = 20000;
		public void credit(double a) {
			balance = balance + a;
			System.out.println("餘額:"+balance);
		}
		public void debit(double b) {
			balance = balance - b;
			System.out.println("餘額:"+balance);
		}
	}
	public static class SavingAccount extends Account {
		public double interestRate;
		public double interest;
		public double setInterestRate() {
			double interestRate = 0.002;
			return interestRate;
		}
		public double getInterestRate() {
			return interestRate;
		}
		public double calculateInterest() {
			interestRate = setInterestRate();
			interest = balance*interestRate;
			System.out.println("利息: "+interest);
			return interest;
		}
	}
	public static class CheckAccount extends Account{
		final double transactionFee = 0.01;
		public double fee;
		public void credit(double a) {
			fee = chargeFee(a);
			balance = balance + a - fee;
			System.out.println("餘額:"+balance);
			System.out.println("手續費:"+fee);
		}
		public void debit(double a) {
			fee = chargeFee(a);
			balance = balance - a - fee;
			System.out.println("餘額:"+balance);
			System.out.println("手續費:"+fee);
		}
		public double chargeFee(double a) {
			fee = a*transactionFee;
			return fee;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("*聯大銀行歡迎您*\n若要使用儲蓄帳戶請輸入1，使用支票帳戶請輸入2:");
		Account a = new Account();
		SavingAccount s = new SavingAccount();
		CheckAccount c = new CheckAccount();
		Scanner input = new Scanner(System.in);
		int m = input.nextInt();
		
		switch(m) {
			case 1:
				System.out.println("存款請輸入3，提款請輸入4:");
				int s1 = input.nextInt();
				if(s1==3) {
					System.out.println("輸入金額: ");
					double m1 = input.nextDouble();
					s.credit(m1);
					s.calculateInterest();
				}else if(s1==4) {
					System.out.println("輸入金額: ");
					double m1 = input.nextDouble();
					s.debit(m1);
					s.calculateInterest();
				}
				break;
			case 2:
				System.out.print("存款請輸入3，提款請輸入4:");
				int s2 = input.nextInt();
				if(s2==3) {
					System.out.println("輸入金額: ");
					double m1 = input.nextDouble();
					c.credit(m1);
					c.chargeFee(m1);
				}else if(s2==4) {
					System.out.println("輸入金額: ");
					double m1 = input.nextDouble();
					c.debit(m1);
					c.chargeFee(m1);
				}
				break;
		}
	}

}
