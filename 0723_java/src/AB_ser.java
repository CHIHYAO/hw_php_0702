import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class AB_ser {
static ServerSocket SS;
static Socket s;
static DataInputStream instream;
static DataOutputStream outstream;
static ArrayList<Integer> range = new ArrayList<Integer>(); //(int)答案池陣列
//static ArrayList<Integer> changeans = new ArrayList<Integer>(); //(int)答案池陣列
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			SS = new ServerSocket(1200);
			
			s = SS.accept();
			DataInputStream instream= new DataInputStream( s.getInputStream() );
			DataOutputStream outstream = new DataOutputStream( s.getOutputStream() );
			
			outstream.writeUTF("GAME START~");
			//(int)答案池
			for(int a=1;a<10;a++){
			      for(int b=0;b<10;b++){
			        if(a==b)continue;
			          for(int c=0;c<10;c++){
			            if(b==c||c==a)continue;
			              for(int d=0;d<10;d++){
			                if(c==d||d==a||d==b)continue;
			                	int ans = a*1000 + b*100 + c*10 + d;
			                	range.add(ans);
			              }
			          }
			      }
			    }
			int rangenum = (int)(Math.random()*range.size());             //傳送猜的數字
            System.out.println("rangenum:" + rangenum);
            outstream.writeUTF("【電腦猜】" + range.get(rangenum)+"\n可能組合->"+range.size());
			while(true) {
				try {
                    ArrayList<Integer> change = new ArrayList<>();
                    System.out.println("可能組合:" + range.size());
                    System.out.println("猜:"+range.get(rangenum));
                    int reA = 0, reB = 0;
                    String result = instream.readUTF();                         //讀取幾A幾B
                    System.out.println(result);
                    String [] re = result.split(",");
                    System.out.println(re[0] + "A" + re[1] + "B") ;
                    reA = Integer.valueOf(re[1]);
                    reB = Integer.valueOf(re[2]);
                    System.out.println("A+B"+(reA+reB));

                    if (re[0].equals("end")){
                        outstream.writeUTF("電腦猜中遊戲結束。");
                        break;
                    }else if ((reA+reB) > 4) {
                        outstream.writeUTF("輸入錯誤請重新輸入。\n請輸入幾A幾B:");
                        System.out.println("Error:"+range.get(rangenum));
                    }else if ((reA+reB) <= 4) {
                    	//縮小範圍
                        for (int i = 0; i < range.size(); i++) {                                     
                            int[] ans = {range.get(i) / 1000, range.get(i) / 100 % 10, range.get(i) / 10 % 10, range.get(i) % 10};
                            int[] guess = {range.get(rangenum) / 1000, range.get(rangenum) / 100 % 10, range.get(rangenum) / 10 % 10, range.get(rangenum) % 10};
                            int A = 0, B = 0;
                            for (int j = 0; j < 4; j++) {
                                for (int k = 0; k < 4; k++) {
                                    if (ans[j] == guess[k]) {
                                        if (j == k) {
                                            A++;
                                        } else {
                                            B++;
                                        }
                                    }
                                }
                            }
                            if (A == Integer.valueOf(re[1]) && B == Integer.valueOf(re[2])) {
                                change.add(ans[0] * 1000 + ans[1] * 100 + ans[2] * 10 + ans[3]);
                            }
                        }
                        range = change;
                        rangenum = (int)(Math.random()*range.size());             //傳送猜的數字
                        System.out.println("rangenum:" + rangenum);
	                    if (range.size() <= 0){
	                    	System.out.println("range.size() <= 0");
	                        outstream.writeUTF("可能組合為0，玩家欺騙電腦，遊戲結束。");
	                        break;
	                    }else {
	                    	outstream.writeUTF("【電腦猜】" + range.get(rangenum)+"\n可能組合->"+range.size());
	                    }
                        
                        

                    }
                }catch (IOException e) {
                    System.out.println("error");
                }
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
