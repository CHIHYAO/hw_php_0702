var mymap = L.map('mapid').setView([24.544, 120.812], 13);

L.tileLayer('https://api.maptiler.com/maps/hybrid/{z}/{x}/{y}.jpg?key=J7nrAhAYxrxm0dp6VyGH', {
  attribution:'<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>'
}).addTo(mymap);

L.Routing.control({
  waypoints: [
    L.latLng(24.544, 120.814),
    L.latLng(24.532, 120.819)
  ],
  routeWhileDragging: true
}).addTo(mymap);
