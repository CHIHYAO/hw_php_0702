var mymap = L.map('mapid').setView([24.544, 120.812], 13);
L.tileLayer('https://api.maptiler.com/maps/hybrid/{z}/{x}/{y}.jpg?key=J7nrAhAYxrxm0dp6VyGH', {
  attribution:'<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>'
}).addTo(mymap);

//////標記//////
var marker = L.marker([24.54, 120.81]).addTo(mymap);

//////畫圓形//////
var circle = L.circle([24.53, 120.83], {
  color:'red',
  fillColor:'#f03',
  fillOpacity:0.5,
  radius:500
}).addTo(mymap);

//////畫矩形//////
var polygon = L.polygon([
  [24.541, 120.79],
  [24.541, 120.80],
  [24.53, 120.80],
  [24.53, 120.79]
]).addTo(mymap);

//////彈出視窗//////
  ///點擊物件的彈跳訊息///
marker.bindPopup("<b>Hello World!</b><br>I am a popup.</br>");
circle.bindPopup("I am a circle.");
polygon.bindPopup("I am a polygon.");
  ///點的彈跳訊息(打開頁面即跳出)///
// var popup = L.popup()
//              .setLatLng([24.544, 120.83])
//              .setContent("I am a standalone popup.")
//              .openOn(mymap);

//////單擊事件//////
function onMapClick(e){
  // alert("You clicked the map at " + e.latlng);
  var popup = L.popup()
               .setLatLng(e.latlng)
               .setContent("You clicked the map at " + e.latlng)
               .openOn(mymap);
}
mymap.on('click', onMapClick);
