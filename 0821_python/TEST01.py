#print("Hello World!")

"""
import keyword as kw
print(kw.kwlist)
"""
'''
str = "Twinkle Twinkle little star"
print(str)	#Twinkle Twinkle little star
print(str[0:15])	#Twinkle Twinkle
print(str[0:-5])	#Twinkle Twinkle little
'''
'''
#input
str = input("Please Input: ")
print(str)	#haha
print(len(str))	#4
print(str[0:(len(str)) - 2])	#ha
'''
'''
while True:
	str = input("Please Input Your Grade(Only 0~100) Or Exit:")
	if(str != "exit"):
		num = int(str)
		if (num >= 90):
			print(str," is Goood!!!")
		elif (num >= 60):
			print(str,"sosooo")
		elif (num < 60):
			print("Never Give Up.")
	else:
		break
'''
#for
for i in range(5):
	print(i)
	
days = ["Sun", 1, "Tue",3, "Tus", 5, "Sat"]
for x in days:
	print(x)