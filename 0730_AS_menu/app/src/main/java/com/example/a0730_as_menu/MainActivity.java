package com.example.a0730_as_menu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    String[] teachers = {"馬麗菁\n教授 兼 管理學院院長\n聯絡電話:037-381510", "張朝旭\n副教授 兼 圖書館系統管理組長\n聯絡電話:037-381520", "陳宇佐\n助理教授\n聯絡電話:037-381513"};
    int[] pic = {R.drawable.teacher04, R.drawable.teacher05, R.drawable.teacher12};
    int[] time = {0,0,0};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.ListView);

        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return teachers.length;     //  取得數量
            }

            @Override
            public Object getItem(int i) {
                return i;        //     取position位置的item
            }

            @Override
            public long getItemId(int i) {
                return i;       //      取position位置的id
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View layout = View.inflate(MainActivity.this, R.layout.layout, null);
                ImageView img = (ImageView)layout.findViewById(R.id.IMG);
                TextView tv1 = (TextView)layout.findViewById(R.id.TV);
                img.setImageResource(pic[i]);
                tv1.setText(teachers[i]);
                return layout;
            }
        };
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                time[i]++;
                Toast.makeText(MainActivity.this, "已點擊"+time[i]+"次", Toast.LENGTH_LONG).show();;
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_0730, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem menuItem){
        int id = menuItem.getItemId();
        if(id==R.id.delete){
            Toast.makeText(MainActivity.this, "刪除", Toast.LENGTH_SHORT).show();
        }
        else if(id==R.id.edit){
            Toast.makeText(MainActivity.this, "修改", Toast.LENGTH_SHORT).show();
        }
        else if(id==R.id.helping){
            Toast.makeText(MainActivity.this, "幫助", Toast.LENGTH_SHORT).show();
        }
        else if(id==R.id.setting){
            Toast.makeText(MainActivity.this, "設定", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
