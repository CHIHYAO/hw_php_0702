import java.util.ArrayList;
import java.util.Scanner;

public class math {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> arr = new ArrayList<String>();
		System.out.println("Input: ");
		Scanner input = new Scanner(System.in);
		String n1 = input.next();
		String n2 = input.next();
				
		arr.add(n1);
		arr.add(n2);
		math m = new math();
		bignumber bn = m.new bignumber();
		bn.plus(n1, n2);
		bn.sub(n1, n2);
		
	}
	class bignumber {
		public void plus(String a, String b){
																//確定A長度小於等於B長度
			if(a.length()>b.length()){
				String t = a;
				a = b;
				b = t;
			}
			int cha=b.length()-a.length();
			for (int i=0; i<cha; i++) {	
				a = '0'+a;   //把a前面的空缺補上0
			}
			String result = "";
			int w = 0;      //定義一個進位的變數
			for (int i=b.length()-1; i>=0 ; i--) {
																//從s1和s2中取出字元，減去48就是 int型別的數字，再加上進位就是當前位的結果
				int ans = b.charAt(i) + a.charAt(i) - 96 + w;
				w = ans/10;   									//把當前計算結果整除10就是  w的進位
				result = (ans % 10) + result;       			//取餘就是當前位應該顯示的數字，把它加在前面就可以了
			}	
																//因迴圈沒有判斷第一位的進位	，所以最後再判斷一次
			if(w==1)result = 1+result;
			System.out.println("PLUS:"+result);
		}
		public void sub(String a, String b) {
																//確定A長度小於等於B長度
			if(a.length()>b.length()){
				String t = a;
				a = b;
				b = t;
			}
			int cha=b.length()-a.length();
			for (int i=0; i<cha; i++) {	
				a = '0'+a;   									//把a前面的空缺補上0
			}
			String result = "";
			int w = 0;
			for (int i=a.length()-1; i>=0; i--) {
																//計算每一個位置的差，在加上借位w的值
				int ans=a.charAt(i)-b.charAt(i)+w;
																//如果c小於0，說明需要借位，c+=10，然後w該為-1，否則，借位w=0
				if(ans<0){
					ans +=10;
					w = -1;
				}else{
					w = 0;
				}
				result=ans+result;    // 把當前位的數字放入result裡
			}
			System.out.println("SUB:"+result);
		}
	}
}
