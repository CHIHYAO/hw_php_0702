import java.util.HashMap;
import java.util.Scanner;

public class hashmap {
static HashMap<String,String> map = new HashMap<String,String>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Input: ");
		Scanner input = new Scanner(System.in);
		String p=input.next();
		String[] p1 = p.split(",");
		hashmap hm = new hashmap();
		
		if(p1.length==1) {
			//oneplace
			onep one = hm.new onep();
			one.show(p1[0]);
		}else if(p1.length==2) {
			//twoplace
			twop two = hm.new twop();
			two.time(p1[0], p1[1]);
		}
	}
	class twop{
		public void time(String a, String b) {
			map.put("東京迪士尼","139°52E");
			map.put("台北101","121°33E");
			map.put("美國黃石國家公園","110°30W");
			map.put("法國艾菲爾鐵塔","2°17E");
			
			String[] t1 = map.get(a).split("°");
			String[] t2 = map.get(b).split("°");
			int t11 = Integer.valueOf(t1[0]);
			int t22 = Integer.valueOf(t2[0]);
			int time = (t11-t22)/15;
			System.out.println("時差: "+time);
				
		}
	}
	class onep{
		public void show(String a) {
			map.put("東京迪士尼","35°37N,139°52E");
			map.put("台北101","25°02N,121°33E");
			map.put("美國黃石國家公園","44°36N,110°30W");
			map.put("法國艾菲爾鐵塔","48°51N,2°17E");
			System.out.println(map.get(a));
		}
	}
}
