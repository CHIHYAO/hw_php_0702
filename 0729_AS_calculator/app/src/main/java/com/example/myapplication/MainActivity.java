package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView TV1, TVANS;
    Button BT0, BT1, BT2, BT3, BT4, BT5, BT6, BT7, BT8, BT9, BTc, BTa, BTs, BTX, BTt, BTe;
    int  firstnumber, secondnumber;
    String total = "", modle;
    String num = "";
    String[] split_line; //陣列要這樣存


    @Override
    protected void onCreate(Bundle savedInstanceState) {    //Main
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view();
        getButton();
    }

    @Override
    public void onClick(View view) {
       switch (view.getId()){
           case R.id.bt0:
                total +="0";
                TV1.setText(total);
                break;
           case R.id.bt1:
                total +="1";
                TV1.setText(total);
               break;
           case R.id.bt2:
               total +="2";
               TV1.setText(total);
               break;
           case R.id.bt3:
               total +="3";
               TV1.setText(total);
               break;
           case R.id.bt4:
               total +="4";
               TV1.setText(total);
               break;
           case R.id.bt5:
               total +="5";
               TV1.setText(total);
               break;
           case R.id.bt6:
               total+="6";
               TV1.setText(total);
               break;
           case R.id.bt7:
               total +="7";
               TV1.setText(total);
               break;
           case R.id.bt8:
               total +="8";
               TV1.setText(total);
               break;
           case R.id.bt9:
               total +="9";
               TV1.setText(total);
               break;
           case R.id.bta:
               total +="+";
               modle = "add";
               TV1.setText(total);
               break;
           case R.id.bts:
               total +="-";
               modle ="subtract";
               TV1.setText(total);
               break;
           case R.id.btX:
               total +="*";
               modle ="multiply";
               TV1.setText(total);
               break;
           case R.id.bt:
               total+="/";
               modle ="minus";
               TV1.setText(total);
               break;
           case R.id.btc:
               total="";
               TV1.setText(total);
               break;
           case R.id.bte:
               num = TV1.getText().toString();

               split_line = num.split("\\+|-|\\*|/");

               Log.e("aaa",num);
               firstnumber = Integer.parseInt(split_line[0]);
               Log.e("mum1",split_line[0]);
               secondnumber = Integer.parseInt(split_line[1]);
               Log.e("mum2",split_line[1]);

               if( firstnumber!=0&&secondnumber!=0 ){
                    if( modle.equals("add") ){
                        String ans;
                        ans = Integer.toString( firstnumber+secondnumber );
                        TVANS.setText(ans);
                    }
                   if( modle.equals("subtract") ){
                       String ans;
                       ans = Integer.toString( firstnumber-secondnumber );
                       TVANS.setText(ans);
                   }
                   if( modle.equals("multiply") ){
                       String ans;
                       ans = Integer.toString( firstnumber*secondnumber );
                       TVANS.setText(ans);
                   }
                   if( modle.equals("minus") ){
                       String ans;
                       ans = Integer.toString( firstnumber/secondnumber );
                       TVANS.setText(ans);
                   }
               }
               break;

       }
    }

    public void view(){

        TV1 = findViewById(R.id.tv1);
        TVANS = findViewById(R.id.tva);
        BT0 = findViewById(R.id.bt0);
        BT1 = findViewById(R.id.bt1);
        BT2 = findViewById(R.id.bt2);
        BT3 = findViewById(R.id.bt3);
        BT4 = findViewById(R.id.bt4);
        BT5 = findViewById(R.id.bt5);
        BT6 = findViewById(R.id.bt6);
        BT7 = findViewById(R.id.bt7);
        BT8 = findViewById(R.id.bt8);
        BT9 = findViewById(R.id.bt9);
        BTa = findViewById(R.id.bta);
        BTs = findViewById(R.id.bts);
        BTX = findViewById(R.id.btX);
        BTt = findViewById(R.id.bt);
        BTe = findViewById(R.id.bte);
        BTc = findViewById(R.id.btc);
    }

    public void getButton(){
        BT0.setOnClickListener(this);
        BT1.setOnClickListener(this);
        BT2.setOnClickListener(this);
        BT3.setOnClickListener(this);
        BT4.setOnClickListener(this);
        BT5.setOnClickListener(this);
        BT6.setOnClickListener(this);
        BT7.setOnClickListener(this);
        BT8.setOnClickListener(this);
        BT9.setOnClickListener(this);
        BTa.setOnClickListener(this);
        BTs.setOnClickListener(this);
        BTX.setOnClickListener(this);
        BTt.setOnClickListener(this);
        BTe.setOnClickListener(this);
        BTc.setOnClickListener(this);
    }
}
