function mouseDragged(){
  boxB.push(new Box(mouseX, mouseY, random(20, 30), random(20, 30)));
}

function draw(){
  background(125);
  rectMode(CENTER);
  rect(200, height, width, 10);
  for(var i=0; i<boxB.length; i++){
    boxB[i].show();
  }

  function Boundary(x,y,w,h,a){
    var options = {
      friction:0,
      restitution:0.8,
      angle:a,
      isStatic:true
    }
    this.w = w;
    this.h = h;
    World.add(world, this.body);

    this.show = function(){
      var pos = this.body.position;
      var angle = this.body.angle;
      push();
        translate(pos.x, pos.y);
        rotate(angle);
        rectMode(CENTER);
        rect(0,0,this.w, this.h);
      pop();

;    }
  }
}
