function setup() {
  // createCanvas(400, 400);
  noCanvas();

  wordsize = createP("請輸入字體大小:");
  wordsize.id("wordone");
  wordsize.child(inp);
  wordsize.attribute('align', 'left');

  var inp = createInput('');
  inp.id("Input");
  inp.style('100px');
  inp.style('width', '150px');
  inp.attribute('align', 'left');

  var color = createColorPicker('#ff0000');
  color.id("Color");
  color.input(setShade1);
  color.style('margin-left', '100px');
  color.attribute('align', 'left');
  // setMidShade();

  var button = createButton('click me');
  button.style('margin-left', '100px');
  button.mousePressed(changeBG);
}

function setMidShade() {
  // Finding a shade between the two
  fill(color);
  rect(20, 20, 60, 60);
}

function setShade1() {
  setMidShade();
  console.log('You are choosing shade 1 to be : ', this.value());
}

function changeBG() {
  var w = document.getElementById('wordone');
  var s = document.getElementById('Input').value;
  // var c = document.getElementById('Color').value;
  w.style.fontSize = s + 'px';
  // wordone.
  console.log("1:"+w);
  console.log("2:"+s);
  console.log("3:"+w.style.fontSize);
}
