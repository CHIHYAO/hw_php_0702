import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class S_manyp {
static ServerSocket SS;
static Socket c1;
static DataInputStream instream; 		//接收聊天句子
static DataInputStream instreamname; 	//接收名字
static DataOutputStream outstream; 		//傳送聊天句子
static ArrayList <String>na = new ArrayList<String>(); 			//存放名字
static ArrayList <clienttalk>t = new ArrayList<clienttalk>(); 	//存放聊天句子

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {		
			SS = new ServerSocket(711);
//			Scanner input = new Scanner(System.in);
						
			System.out.println( "Server" );

/*接收客戶端訊息*/		
			while(true) {
				try {
					c1 = SS.accept();
					clientin in = new clientin(c1);
					in.start();		//接收與傳送函式
				}catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}catch(IOException e) {
			System.out.println( "ERROR" );
		}
	}
/*clientname*/	
	public static class clientin extends Thread{
		
        Socket socket;
        DataOutputStream outstream;
        DataInputStream instream;
        clientin (Socket a)throws IOException{		//建構子
        	this.socket = a;		//因為有不同的使用者，所以會要分出來
        	instreamname = new DataInputStream( socket.getInputStream() );
			outstream = new DataOutputStream( socket.getOutputStream() );
        }
		public void run() {	
			try {
				 
				String name = instreamname.readUTF();	
				System.out.println(name+" Coming...");
				na.add(name);			//存入名字
				clienttalk talk = new clienttalk(socket, name);
				t.add(talk);	//存入聊天句子
				talk.start();
					for(int i=0; i<na.size(); i++) {
						if(na.get(i).equals(name))		//判斷登入者是誰
						{
							t.get(i).outstream.writeUTF("你加入聊天...");
						}else {
							t.get(i).outstream.writeUTF(name+"加入聊天");
						}				
					}
				} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
		}
	}
/*clienttalk*/
	public static class clienttalk extends Thread{
		String serverlisten;
		String serversend;
        Socket socket;
        String un;
        DataOutputStream outstream;
        DataInputStream instream;
        clienttalk (Socket a, String n)throws IOException{		//建構子
        	this.un = n;		//該使用者名字
        	this.socket = a;	//該使用者的分流
        	instream = new DataInputStream( socket.getInputStream() );
			outstream = new DataOutputStream( socket.getOutputStream() );
        }
		public void run() {	
			try {
					while(true) {						
						serverlisten = instream.readUTF();
						System.out.println("Client: "+serverlisten);
						String[] sl = serverlisten.split(",");
						if(sl[0].equals("88")) {		//輸入88離開聊天室
							for(int i=0; i<na.size(); i++) {
								if(!na.get(i).equals(un)){//////其他使用者顯示有人離開
									t.get(i).outstream.writeUTF(un+":"+sl[1]);
									t.get(i).outstream.writeUTF(un+"離開了聊天室...");
								}			
							}
							int b=na.indexOf(un);
							na.remove(b);
							t.remove(b);
							socket.close();
							break;
						}
						else if(sl[0].equals("who")) {
							int b = na.indexOf(un);								
								t.get(b).outstream.writeUTF("【在線成員】"+na);
						}
						else if(sl[0].equals("send")) {
							 System.out.println("準備接收檔案");
							   InputStream fisin=socket.getInputStream();
							            String filename="newFile.docx";
							            System.out.println("接收檔案的新檔名為:"+filename);
							            File file=new File(filename);	//在JAVA建立一個被接收的檔案
							            InputStream in=new DataInputStream(new BufferedInputStream(fisin));		//BUF網路站存資料的地方
							            RandomAccessFile raf=new RandomAccessFile(file,"rw");
							            System.out.println("開始接收檔案");
							            byte[] buf=new byte[2048];
							            int num=in.read(buf);
//							            System.out.println("numb: "+num);
							            while(num>=(-1)){
							             raf.write(buf,0,num);	//將網路數據寫往文件
							             raf.skipBytes(num);	//依照順序寫下文件的字
							                num=in.read(buf);	//繼續從網路中讀取文件
							                System.out.println("numa: " + num);
//							                if(num>=buf.length)break;
							            }
							            System.out.println("接收檔案完成");
							            in.close();
							            raf.close();
						}
						else {									
							///////顯示聊天句子【含群聊、私聊】
							for(int i=0; i<na.size(); i++) {
								if(na.get(i).equals(sl[0])) {
									///////私聊判斷
									System.out.println("【私訊】"+un+"對你:"+sl[1]);
									int b = na.indexOf(sl[0]);
									t.get(b).outstream.writeUTF("【私訊】"+un+"對你:"+sl[1]);
								}
								else if(na.get(i).equals(un)) {
									///////該使用者的聊天句子
								}
								else if(sl.length<2&&!na.get(i).equals(un)){		
									////////【群聊】顯示對話
									t.get(i).outstream.writeUTF(un+":"+serverlisten);
								}
							}
						}
					}
				} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
		}
	}
}
